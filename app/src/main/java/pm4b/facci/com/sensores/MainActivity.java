package pm4b.facci.com.sensores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnSensor1, btnSensor2, btnSensor3, btnVibrador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSensor1 = (Button) findViewById(R.id.btnSensor1);
        btnSensor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        btnSensor2 = (Button) findViewById(R.id.btnSensor2);
        btnSensor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadSensorBrujula.class);
                startActivity(intent);
            }
        });

        btnSensor3 = (Button) findViewById(R.id.btnSensor3);
        btnSensor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadSensorAproximidad.class);
                startActivity(intent);
            }
        });

        btnVibrador = (Button) findViewById(R.id.btnVibrador);
        btnVibrador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadVibrador.class);
                startActivity(intent);
            }
        });
    }
}